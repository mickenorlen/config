#!/bin/bash
############################
# 
# This script creates symlinks for all the configuration files in "home" to your "~/"; maintaining their structure. All folders and files that already exist will be moved to "old_files; except for the containerDirs as they may contain a multitude of configurations. 
############################


containerDirs=".config .local .local/share .themes .icons" # Directories in home that will not be replaced; only their subfolders might be.

homeDirs=`find ./home -maxdepth 1 -type d | cut -d '/' -f3-` # Directories in home that will be replaced
homeFiles=`find ./home -maxdepth 1 -type f | cut -d '/' -f3-` # Files in home that will be replaced

# Create empty arrays to fill with folders that will be replaced
replaceHomeDirs=()
ignoreHomeDirs=('hej')
replaceHomeFiles=()
replaceSubDirs=()

#Try to create container dirs...



# All files in home.
for f in `echo "$homeFiles"`; do
    echo "homeFiles $f"
     # do something on $f
done


# Check if any folder in the home directory is among containerDirs; if so; don't replace it.
for d in `echo "$homeDirs"`; do
	
	exist=false
    ignored=0
	for check in `echo "$containerDirs"`; do
        # If it exists among containerDirs, echo that it's ignored
		if [ "$d" = "$check" ]; then
			exist=true
            echo -e "\e[31mIgnored homeDir $d\e[0m"
          #  ${ignoreHomeDirs[${#ignoreHomeDirs[@]}=$d
#            set ignoreHomeDirs[$ignored]='foo'
# TODO Remember .vim to root also
            array=()
            array+=('hej')
                     echo $array
          # set ignoreHomeDirs[$[${#ignoreHomeDirs[@]}+1]]=$d
           # ignoreHomeDirs[${#ignoreHomeDirs[*]}]=$d
    fi
	done
    # If does not exist among containerDirs, add to array for replacing
	if [ "$exist" = false ]; then
		replaceHomeDirs+=("$d")
	fi
done


# Subfolders of containerDirs that will be replaced
for subDir in `echo "$containerDirs"`; do
    for eachSub in `echo "$(find "./home/$subDir" -mindepth 1 -maxdepth 1 -type d | cut -d '/' -f3-)"`; do
    
        # Check if any subDir is among containerDirs; if so; don't replace it. 
	     for d in `echo "$eachSub"`; do
         
            exist=false
            for check in `echo "$containerDirs"`; do
                # If it exists echo that it's ignored in red
                if [ "$d" = "$check" ]; then
                    exist=true
                    echo -e "\e[31mignored $d\e[0m"
                fi
            done
            # If does not exist among containerDirs, 
            if [ "$exist" = false ]; then
                echo "subDirs $d"
            fi
        done
	done
done

                     echo -e "\n$ignored"
                     echo $ignoreHomeDirs
                     

echo -e "\n"
echo "Replaced homeDirs $replaceHomeDirs"
echo -e "\e[31mIgnored homeDirs $ignoreHomeDirs\e[0m"
