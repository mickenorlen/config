" Load pathogen and vim-ipi
call pathogen#runtime_append_all_bundles()
call pathogen#helptags()
call ipi#inspect()

" Disable beep
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=

" Create folds with F9 and auto save/load
nnoremap <silent> <F9> @=(foldlevel('.')?'za':"\<F9>")<CR>
vnoremap <F9> zf
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview 

" Highlighting
syntax on                           
filetype on               
filetype plugin indent on  
filetype plugin on
set nocompatible

" Tabspace=4
set ts=4
set tabstop=4
set softtabstop=4
set shiftwidth=4
set linebreak
let tex_no_error=1
set gcr=n:blinkon0
colorscheme phpx
set ignorecase
set smartcase

" Line numbering
set number
set numberwidth=3
" Numbercolor
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE
" SpeelBadcolor
highlight SpellBad term=reverse ctermbg=4


" Enable open at same position as when closed.
if has("autocmd")
  " When editing a file, always jump to the last cursor position
  autocmd BufReadPost *
  \ if line("'\"") > 0 && line ("'\"") <= line("$") |
  \   exe "normal g'\"" |
  \ endif
endif

" no swap files
set noswapfile

" Enable folding (for methods =  part of classes)
set foldmethod=indent
set foldlevel=99


" Remap slit to new window and switching window when split with <c-w>s/v
map <c-w>s :topleft new<CR>
map <c-w>S :leftabove split<CR>
map <c-w>v :botright vnew<CR>
map <c-w>V :rightbelow vsplit<CR>

map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h 



" Load plugins from .vim/ipi
" Tasklist (todo, fixme) Gundo (review all old saves)
map <leader>td :silent! IP Tasklist<CR>: <Plug>TaskList
map <leader>g :silent! IP gundo<CR>:GundoToggle<CR>


let g:ConqueTerm_FastMode = 0
" Conqueterm start in splitbelow window
map <leader>c :silent! IP ConqueTerm<CR>:exe 'ConqueTermSplit bash' \| redraw!<CR>

" Makegreen (test script) pep8 for script inconsistency
map <leader>G <Plug>MakeGreen
map <leader>G :silent! IP makegreen<CR>: <Plug>MakeGreen<CR>
map <leader>e :silent! IP nerdtree<CR>: NERDTreeToggle<CR>
let g:pep8_map='<leader>8'


" Disable pyflakes popup shit
let g:pyflakes_use_quickfix=0


" Context sensetive tabbing
au FileType python set omnifunc=pythoncomplete#Complete
let g:SuperTabDefaultCompletionType = "context"
set completeopt=menuone,longest,preview


" Egna hotkeys
" Update snippets and vimrc 
map <leader>R :so $MYVIMRC \|:call ReloadAllSnippets()<CR>
" Redraw buffer
map <leader>r :redraw!<CR>
" Insert ONE character
map <Space> i_<Esc>r
" Toggle indent etc for pasting
set pastetoggle=<F2>
" Delete current buffer \d
map <leader>d :bd \| redraw!<CR>
" Toggle open close folds and create fold when highlighted
nnoremap <tab> zA
vnoremap <tab> zf

autocmd FileType tex map <f4> :silent IP ConqueTerm<CR>:silent exe 'ConqueTermSplit bash -c ''make; bash''' \| redraw!<CR>
autocmd FileType python map <f5> :silent IP ConqueTerm<CR>:silent exe 'ConqueTermSplit bash -c ''python2 '''.expand('%:p').'''; python2 2>/dev/null''' \| redraw!<CR>
autocmd FileType java map <f5> :Java<CR>


" Close splitwindows
map fc <Esc>:call CleanClose(1)
map fq <Esc>:call CleanClose(0)

function! CleanClose(tosave)
if (a:tosave == 1)
	w!
endif
let todelbufNr = bufnr("%")
let newbufNr = bufnr("#")
if ((newbufNr != -1) && (newbufNr != todelbufNr) && buflisted(newbufNr))
        exe "b".newbufNr
else
	bnext
endif

if (bufnr("%") == todelbufNr)
        new endif
exe "bd".todelbufNr
endfunction
