# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
#PS1='[\u@\h \W]\$ '


#********************************
 # Redirect dropbox-cli commands
 # to change Dropbox's home path 
#********************************

#dropbox_path="/mnt/data"

#user_path="/home/`logname`"
#dropbox_cli='/usr/bin/dropbox'
#set_home_dropbox="HOME=$dropbox_path"
#set_home_home="HOME=$user_path"

#alias dropbox="read -p '[Dropbox@'"$dropbox_path"']> ' input;\
#$set_home_dropbox; $dropbox_cli \$input; $set_home_home;"

# php brew
[[ -e ~/.phpbrew/bashrc ]] && source ~/.phpbrew/bashrc

PS1='\n \[\e[2;37m\]┌─[\[\e[1;36m\] \d \[\e[1;31m\]\T \[\e[1;37m\]] \n\[\e[1;37m\] └─[ \[\e[1;34m\]@ \[\e[1;32m\]\w \[\e[1;37m\]]\[\e[1;35m\]---> \[\e[0;37m\]'
eval "`dircolors`"
export LS_COLORS=`echo $LS_COLORS|sed 's/34\;42/0/g'`

# GEM jekyll
 GEM_HOME=$(ls -t -U | ruby -e 'puts Gem.user_dir')
 GEM_PATH=$GEM_HOME
 export PATH=$PATH:$GEM_HOME/bin

export EDITOR="vim"

alias rvim='sudo -E vim'
alias grep='grep --color -HA2'

# List npm modules
alias npmls="npm list -g --depth=0"

# Rename web files to lowercase and space to _
alias rename-files="perl-rename -n 's/ +\././; y/A-Z _/a-z-/ if -f;' ./*"
alias rename-files-exec="perl-rename 's/ +\././; y/A-Z _/a-z-/ if -f;' ./*"
alias rename-files-recursive='find . -depth -type f -name "*" -exec perl-rename -n "s/ +\././; y/A-Z _/a-z-/" {} +'
alias rename-files-recursive-exec='find . -depth -type f -name "*" -exec perl-rename "s/ +\././; y/A-Z _/a-z-/" {} +'
alias rename-folders="perl-rename -n 's/ +\././; y/A-Z _/a-z-/ if -d;' ./*"
alias rename-folders-exec="perl-rename 's/ +\././; y/A-Z -/a-z_/ if -d;' ./*"
alias rename-folders-recursive='find . -depth -type d -name "*" -exec perl-rename -n "s/ +\././; y/A-Z -/a-z_/" {} +'
alias rename-folders-recursive-exec='find . -depth -type d -name "*" -exec perl-rename "s/ +\././; y/A-Z -/a-zr_/" {} +'

# WWW 2017-09-02
alias serverwork="sudo gvim -O /etc/hosts /etc/httpd/conf/httpd.conf /etc/php/php.ini /etc/httpd/conf/extra/httpd-vhosts.conf"
alias projects="cd /home/micke/Git/www/projects; ls -1"
