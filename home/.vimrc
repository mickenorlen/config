" Highlighting
syntax on
" Tabspace=4
set ts=4
set tabstop=4
set softtabstop=4
set shiftwidth=4
set linebreak
colorscheme slate
set ignorecase
set smartcase

" Line numbering
set number
set numberwidth=3
" Numbercolor
highlight LineNr term=bold cterm=NONE
" ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE
" SpeelBadcolor
highlight SpellBad term=reverse ctermbg=4


" Enable open at same position as when closed.
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

" no swap files
set noswapfile

" Egna hotkeys
" Redraw buffer
map <leader>r :redraw!<CR>
" Insert ONE character
map <Space> i_<Esc>r
" Toggle indent etc for pasting
set pastetoggle=<F2>
